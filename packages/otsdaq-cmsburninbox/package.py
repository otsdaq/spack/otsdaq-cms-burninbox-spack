# Copyright 2013-2023 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install otsdaq-cmsburninbox
#
# You can edit this file again by typing:
#
#     spack edit otsdaq-cmsburninbox
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

import os
import sys

from spack import *

def sanitize_environments(env, *vars):
    for var in vars:
        env.prune_duplicate_paths(var)
        env.deprioritize_system_paths(var)

class OtsdaqCmsburninbox(CMakePackage):
    """The toolkit is used to control the Fermilab Burnin box for the CMS tracker project."""

    # Add a proper url for your package's homepage here.
    homepage = "https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox"
    url = "https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox.git"
    git = "https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox.git"

    # Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers("github_user1", "github_user2")

    # Add the SPDX identifier of the project's license below.
    # See https://spdx.org/licenses/ for a list.
    license("UNKNOWN")

    # Add proper versions and checksums here.
    # version("1.2.3", md5="0123456789abcdef0123456789abcdef")
    version("latest", branch="develop", get_full_repo=True)
    version("frozen", commit="b5677e38234e5878cb24fddb7177cb92312ff5ef", get_full_repo=True)
   

    # Add dependencies if required.
    depends_on("cetmodules", type="build")
    depends_on("otsdaq")
    depends_on("otsdaq-utilities")

    # def cmake_args(self):
    #     # FIXME: Add arguments other than
    #     # FIXME: CMAKE_INSTALL_PREFIX and CMAKE_BUILD_TYPE
    #     # FIXME: If not needed delete this function
    #     args = []
    #     return args

    def setup_build_environment(self, env):
        env.set("OTSDAQ_CMSBURNINBOX_DIR", self.prefix)
    #def setup_dependent_build_environment(self, env):

    def setup_run_environment(self, env):
        prefix = self.prefix
        # Set the main directory where we can find the burnin box installed package
        env.set("OTSDAQ_CMSBURNINBOX_DIR", prefix)
        # Ensure we can find the libraries
        env.set("OTSDAQ_CMSBURNINBOX_LIB", prefix.lib)
        # Ensure we can find the binaries
        env.set("OTSDAQ_CMSBURNINBOX_BIN", prefix.bin)
        # Ensure we can find the plugin libraries.
        env.prepend_path("CET_PLUGIN_PATH", prefix.lib)


    def setup_dependent_run_environment(self, env, dependent_spec):
        prefix = self.prefix
        # Ensure we can find plugin libraries.
        env.prepend_path("CET_PLUGIN_PATH", prefix.lib)
